package az.atl.mstests;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsTestsApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsTestsApplication.class, args);
    }

}
