package az.atl.mstests.model.mapper;

import az.atl.mstests.dao.entity.StudentEntity;
import az.atl.mstests.model.StudentDTO;
import az.atl.mstests.util.PhoneNumberMaskUtil;

public enum StudentMapper {
    STUDENT_MAPPER;

    public StudentDTO mapEntToDto(StudentEntity entity) {
        return StudentDTO.builder()
                .id(entity.getId())
                .fullName(entity.getFullName())
                .phoneNumber(PhoneNumberMaskUtil.maskPhoneNumber(entity.getPhoneNumber()))
                .build();
    }

    public StudentEntity mapDtoToEnt(StudentDTO dto) {
        return StudentEntity.builder()
                .id(dto.getId())
                .fullName(dto.getFullName())
                .phoneNumber(dto.getPhoneNumber())
                .build();
    }

}
