package az.atl.mstests.service;

import az.atl.mstests.model.StudentDTO;

import java.util.List;

public interface StudentService {

    long createStudent(StudentDTO studentDTO);

    void deleteStudent(Long id);

    StudentDTO getStudentById(Long id);

    List<StudentDTO> getStudents();
}
