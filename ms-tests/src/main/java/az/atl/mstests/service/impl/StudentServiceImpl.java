package az.atl.mstests.service.impl;

import az.atl.mstests.dao.repository.StudentRepo;
import az.atl.mstests.exception.StudentNotFoundException;
import az.atl.mstests.model.StudentDTO;
import az.atl.mstests.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static az.atl.mstests.model.mapper.StudentMapper.STUDENT_MAPPER;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepo studentRepo;

    @Override
    public long createStudent(StudentDTO studentDTO) {
        var ent = STUDENT_MAPPER.mapDtoToEnt(studentDTO);
        var saveEnt = studentRepo.save(ent);
        return saveEnt.getId();
    }

    @Override
    public void deleteStudent(Long id) {
        studentRepo.deleteById(id);
    }

    @Override
    public StudentDTO getStudentById(Long id) {
        var studentEnt = studentRepo.findById(id).orElseThrow(
                () -> {
                    throw new StudentNotFoundException("Student Not Found");
                }
        );
        return STUDENT_MAPPER.mapEntToDto(studentEnt);
    }

    @Override
    public List<StudentDTO> getStudents() {
        return studentRepo.findAll().stream()
                .map(STUDENT_MAPPER::mapEntToDto)
                .collect(Collectors.toList());
    }
}
