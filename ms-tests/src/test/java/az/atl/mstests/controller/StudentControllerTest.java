package az.atl.mstests.controller;

import az.atl.mstests.model.StudentDTO;
import az.atl.mstests.service.StudentService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class StudentControllerTest {

    @InjectMocks
    private StudentController studentController;

    @Mock
    private StudentService studentService;

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testGetUserById_api() {
        ResponseEntity<StudentDTO> response = restTemplate
                .getForEntity("http://localhost:" + port + "/student/1", StudentDTO.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void testGetUserById_() {
        var expectedDto = StudentDTO.builder()
                .id(2L)
                .phoneNumber("123")
                .fullName("John Doe").build();
        when(studentService.getStudentById(2L)).thenReturn(expectedDto);

        StudentDTO studentDTO = studentController.getById(2l);
        assertEquals(expectedDto.getId(), studentDTO.getId());
        assertEquals(expectedDto.getFullName(), studentDTO.getFullName());
    }

    @Test
    public void testGetUserById_ifNull() {
        var expectedDto = StudentDTO.builder()
                .id(2L)
                .phoneNumber("123")
                .fullName("John Doe").build();
        when(studentService.getStudentById(2L)).thenReturn(expectedDto);

        StudentDTO studentDTO = studentController.getById(3l);
        assertNull(studentDTO);
    }
}
