package az.atl.mstests.model;

import az.atl.mstests.dao.entity.StudentEntity;
import az.atl.mstests.model.mapper.StudentMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static az.atl.mstests.util.PhoneNumberMaskUtil.maskPhoneNumber;

public class StudentMapperTest {
    @Test
    public void testMapEntToDto() {
        // Create a sample StudentEntity
        StudentEntity entity = new StudentEntity();
        entity.setId(1L);
        entity.setFullName("Namiq Aliyev");
        entity.setPhoneNumber("+994501231212");

        // Map the entity to DTO
        StudentDTO dto = StudentMapper.STUDENT_MAPPER.mapEntToDto(entity);

        // Validate the mapping
        Assertions.assertEquals(entity.getId(), dto.getId());
        Assertions.assertEquals(entity.getFullName(), dto.getFullName());
        Assertions.assertNotEquals(entity.getPhoneNumber(), maskPhoneNumber(dto.getPhoneNumber()));
    }

    @Test
    public void testMapDtoToEnt() {
        // Create a sample StudentDTO
        StudentDTO dto = StudentDTO.builder()
                .id(1L)
                .fullName("Namiq Aliyev")
                .phoneNumber("+994551234545")
                .build();

        // Map the DTO to entity
        StudentEntity entity = StudentMapper.STUDENT_MAPPER.mapDtoToEnt(dto);

        // Validate the mapping
        Assertions.assertEquals(dto.getId(), entity.getId());
        Assertions.assertEquals(dto.getFullName(), entity.getFullName());
        Assertions.assertEquals(dto.getPhoneNumber(), entity.getPhoneNumber());
    }
}
