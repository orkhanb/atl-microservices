package az.atl.mstests.service;

import az.atl.mstests.dao.entity.StudentEntity;
import az.atl.mstests.dao.repository.StudentRepo;
import az.atl.mstests.exception.StudentNotFoundException;
import az.atl.mstests.model.StudentDTO;
import az.atl.mstests.service.impl.StudentServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
public class StudentServiceTest {
    @InjectMocks
    private StudentServiceImpl studentService;
    @Mock
    private StudentRepo studentRepo;

    @Test
    public void testCreateStudent() {
        // Prepare test data
        StudentDTO studentDTO = StudentDTO.builder()
                .fullName("Orkhan Bakhshiyev")
                .phoneNumber("1234567").build();

        StudentEntity savedEntity = StudentEntity.builder()
                .id(1L)
                .fullName("Orkhan Bakhshiyev")
                .phoneNumber("1234567").build();
        when(studentRepo.save(any(StudentEntity.class))).thenReturn(savedEntity);

        // Perform the test
        long createdId = studentService.createStudent(studentDTO);

        // Verify the results
        assertEquals(1L, createdId);
        verify(studentRepo, times(1)).save(any(StudentEntity.class));
    }

    @Test
    public void testDeleteStudent() {
        // Prepare test data
        Long studentId = 1L;

        // Perform the test
        studentService.deleteStudent(studentId);

        // Verify the results
        verify(studentRepo, times(1)).deleteById(studentId);
    }

    @Test
    public void testGetStudentById_ExistingId() {
        // Prepare test data
        Long studentId = 1L;
        StudentEntity studentEntity = StudentEntity.builder()
                .id(1L)
                .fullName("Orkhan Bakshiyev").build();
        when(studentRepo.findById(studentId)).thenReturn(Optional.of(studentEntity));

        // Perform the test
        StudentDTO studentDTO = studentService.getStudentById(studentId);

        // Verify the results
        assertNotNull(studentDTO);
        assertEquals(studentId, studentDTO.getId());
        assertEquals("Orkhan Bakshiyev", studentDTO.getFullName());
        verify(studentRepo, times(1)).findById(studentId);
    }

    @Test
    public void testGetStudentById_NonExistingId() {
        // Prepare test data
        Long studentId = 1L;
        when(studentRepo.findById(studentId)).thenReturn(Optional.empty());

        // Perform the test and verify the expected exception
        assertThrows(StudentNotFoundException.class, () -> {
            studentService.getStudentById(studentId);
        });

        // Verify the results
        verify(studentRepo, times(1)).findById(studentId);
    }

    @Test
    public void testGetStudents() {
        // Prepare test data
        List<StudentEntity> studentEntities = Arrays.asList(
                new StudentEntity(1L, "Orkhan Bakhshiyev","+994501234567"),
                new StudentEntity(2L, "Elmir Huseynov","+994501234567")
        );
        when(studentRepo.findAll()).thenReturn(studentEntities);

        // Perform the test
        List<StudentDTO> studentDTOs = studentService.getStudents();

        // Verify the results
        assertNotNull(studentDTOs);
        assertEquals(2, studentDTOs.size());
        assertEquals(1L, studentDTOs.get(0).getId());
        assertEquals("Orkhan Bakhshiyev", studentDTOs.get(0).getFullName());
        assertEquals(2L, studentDTOs.get(1).getId());
        assertEquals("Elmir Huseynov", studentDTOs.get(1).getFullName());
        verify(studentRepo, times(1)).findAll();
    }


}
