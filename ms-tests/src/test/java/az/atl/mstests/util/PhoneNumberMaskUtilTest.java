package az.atl.mstests.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

public class PhoneNumberMaskUtilTest {

    @InjectMocks
    private PhoneNumberMaskUtil phoneNumberMaskUtil;

    @Test
    public void testMaskPhoneNumber() {
        // Test case 1: Valid phone number
        var phoneNumber = "1234567890123";
        var expectedMaskedPhoneNumber = "1234567****23";
        var actualMaskedPhoneNumber = phoneNumberMaskUtil.maskPhoneNumber(phoneNumber);
        Assertions.assertEquals(expectedMaskedPhoneNumber, actualMaskedPhoneNumber);

        // Test case 2: Phone number with fewer digits than required
        var phoneNumber2 = "123456";
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            phoneNumberMaskUtil.maskPhoneNumber(phoneNumber2);
        });

        // Test case 3: Null phone number
        String phoneNumber3 = null;
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            phoneNumberMaskUtil.maskPhoneNumber(phoneNumber3);
        });
    }
}
