package az.atl.microservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AtlMicroservicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(AtlMicroservicesApplication.class, args);
	}

}
